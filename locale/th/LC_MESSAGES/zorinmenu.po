# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Kittiphong Meesawat <ktphong@elec.kku.ac.th>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"shell&keywords=I18N+L10N&component=extensions\n"
"POT-Creation-Date: 2013-10-25 00:37+0000\n"
"PO-Revision-Date: 2013-11-01 19:55+0700\n"
"Last-Translator: Kittiphong Meesawat <ktphong@elec.kku.ac.th>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.6\n"

#: ../../../menu.js:96
msgid "Activities Overview"
msgstr "ภาพรวมกิจกรรม"

#: ../../../menu.js:120
msgid "Power Off"
msgstr "ปิดเครื่อง"

#: ../../../menu.js:145
msgid "Log Out"
msgstr "ออกจากระบบ"

#: ../../../menu.js:170
msgid "Lock"
msgstr "ล็อค"

#: ../../../menu.js:198
msgid "Back"
msgstr "ย้อนกลับ"

#: ../../../menu.js:363
msgid "Favorites"
msgstr "รายการโปรด"

#: ../../../menu.js:783
msgid "Home"
msgstr "บ้าน"

#: ../../../menu.js:848
msgid "Type to search…"
msgstr "ป้อนเพื่อค้นหา..."

#: ../../../menu.js:932
msgid "Software"
msgstr "ซอฟต์แวร์"

#: ../../../menu.js:936
msgid "Settings"
msgstr "ตั้งค่า"
